-- Up
alter table user add column email text;
create unique index user_email_idx on user(email);

-- Down
drop index user_email_idx;
update user set email = null;
