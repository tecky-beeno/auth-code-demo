-- Up
create table user (
  id integer primary key
, code text
, trial integer
);

-- Down
drop table if exists user;
