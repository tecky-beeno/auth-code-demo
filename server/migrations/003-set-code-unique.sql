-- Up
create unique index user_code_idx on user(code);

-- Down
drop index user_code_idx;
