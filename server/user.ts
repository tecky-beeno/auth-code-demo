import express from 'express'
import { db } from './db'
const { floor, random } = Math

let router = express.Router()

export default router

router.get('/user', (req, res) => {
  res.json('user data')
})

router.post('/user/code', (req, res) => {
  let email = req.query.email
  console.log('email:', email)
  if (typeof email !== 'string' || !email) {
    res.status(400).end({ error: "missing string 'email' in req.query" })
    return
  }
  let code = ''
  let count_code = db
    .prepare(`select count(*) from user where code = ?`)
    .pluck()
  for (;;) {
    code = randomCode()
    if (!count_code.get(code)) {
      break
    }
  }
  let row = db.prepare(`select id from user where email = ?`).get(email)
  if (row) {
    db.prepare(`update user set code = :code where id = :id`).run({
      code,
      id: row.id,
    })
  } else {
    db.insert('user', { email, code })
  }
  // send email
  let link = `http://localhost:3000/login?code=` + code
  console.log('send email:', {
    to: email,
    content: `Click <a href="${link}">${link}</a> to login`,
  })
  res.json({ data: 'sent' })
})

router.post('/user/login', (req, res) => {
  let code = req.query.code
  console.log('code:', code)
  if (typeof code !== 'string' || !code) {
    res.status(400).json({ error: "missing string 'code' in req.query" })
    return
  }

  let user_id = db
    .prepare(`select id from user where code = ?`)
    .pluck()
    .get(code)
  if (!user_id) {
    res.status(401).json({ error: 'wrong code' })
    return
  }

  let token = 'token:' + JSON.stringify({ user_id })
  res.json({ data: { token } })
})

function randomCode() {
  let code = ''
  for (let i = 0; i < 6; i++) {
    code += floor(random() * 10)
  }
  return code
}
