import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'

let REACT_APP_API_SERVER = 'http://127.0.0.1:8100'

export default function LoginPage() {
  const [hasRequestCode, setHasRequestCode] = useState(false)
  const [email, setEmail] = useState('')
  //   const [code, setCode] = useState('')

  const location = useLocation()
  const params = new URLSearchParams(location.search)
  const code = params.get('code') || ''

  const history = useHistory()
  function setCode(code: string) {
    history.replace(location.pathname + '?code=' + code)
  }

  async function requestCode() {
    const params = new URLSearchParams()
    params.set('email', email)
    const res = await fetch(REACT_APP_API_SERVER + '/user/code?' + params, {
      method: 'POST',
    })
    const json = await res.json()
    console.log(json)
    setHasRequestCode(true)
  }
  async function sendCode() {
    const params = new URLSearchParams()
    params.set('code', code)
    const res = await fetch(REACT_APP_API_SERVER + '/user/login?' + params, {
      method: 'POST',
    })
    const json = await res.json()
    if (json.error) {
      console.error(json.error)
      return
    }
    localStorage.setItem('token', json.data.token)
    history.push('/home')
  }
  useEffect(() => {
    if (code.length === 6) {
      sendCode()
    }
  }, [code])
  return (
    <div className="page">
      <h1>Login Page</h1>

      <div>
        <label htmlFor="email">Email: </label>
        <input
          type="text"
          id="email"
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
      </div>

      <div>
        <button disabled={!email} onClick={requestCode}>
          Request Code
        </button>
      </div>

      <div hidden={!hasRequestCode}>
        <label htmlFor="code">Code: </label>
        <input
          type="text"
          id="code"
          value={code}
          onChange={e => setCode(e.target.value)}
        />
      </div>

      <div>
        <button disabled={code.length < 6} onClick={sendCode}>
          Send Code
        </button>
      </div>
    </div>
  )
}
