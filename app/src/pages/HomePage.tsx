import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'

let REACT_APP_API_SERVER = 'http://127.0.0.1:8100'

export default function HomePage() {
  return (
    <div className="page">
      <h1>Home Page</h1>
    </div>
  )
}
