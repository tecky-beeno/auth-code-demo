import React from 'react'
import './App.css'
import { Route, Switch, Link, Redirect } from 'react-router-dom'
import LoginPage from './pages/LoginPage'
import HomePage from './pages/HomePage'

function App() {
  return (
    <div className="App">
      <nav>
        <Link to="/login">
          <button>login</button>
        </Link>
      </nav>
      <Switch>
        <Route path="/login" component={LoginPage} />
        <Route path="/home" component={HomePage} />
        <Route path="/">
          <Redirect to="/login" />
        </Route>
      </Switch>
    </div>
  )
}

export default App
